// set up mock data:

let posts=[];
let count=1;

// adding post data

document.querySelector('#form-add-post').addEventListener('submit', (e)=>{
	e.preventDefault();
	// preventDefault - prevents the page from reloading after a button has been clicked/triggered
	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector('#txt-body').value
	});

	count++;

	showPosts(posts);
	alert('Successfully Addded.');
});

// showPosts();
const showPosts=(posts)=>{
	let postEntries='';

	posts.forEach((post)=>{
		// in JS, you can also render HTML elements, gagamitan lang ng function/methods plus appropriate syntax such as template literals (backticks plus ${})
		postEntries+=`
		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost(${post.id})">Edit</button>
			<button onclick="deletePost(${post.id})">Delete</button>
		</div>`
	})
	// the div duplicated the objects inside the postEntries variable
	document.querySelector("#div-post-entries").innerHTML=postEntries;
}

/*
to create  an editPost function that lets us transfer the id, title and body of each post entry to the second form
	when the edit button has been clicked:
		the title of the div will be copies to the title input field of Edit post; same goes for the body
*/

const editPost=(id)=>{
	let title=document.querySelector(`#post-title-${id}`).innerHTML;
	let body=document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value=id;
	document.querySelector('#txt-edit-title').value=title;
	document.querySelector('#txt-edit-body').value=body;
}

// update post:
document.querySelector("#form-edit-post").addEventListener('submit', (e)=>{
	e.preventDefault();

	for(let i=0; i<posts.length; i++){
		// the value of posts[i].id is a Number but the value for document.querySelector('#text-edit-id').value is a string kaya lalagyan ng toString kasi === dapat pareho sila ng form. otherwise hindi macocompare yung two values and hindi mag-uupdate
		if(posts[i].id.toString()===document.querySelector('#txt-edit-id').value){
			posts[i].title=document.querySelector('#txt-edit-title').value;
			posts[i].body=document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Successfully updated.');

			break;
		}
	}
});

// Activity
// Delete post

 const deletePost=(id)=>{
	let title=document.querySelector(`#post-title-${id}`).innerHTML;
	let body=document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-delete-id').value=id;
	document.querySelector('#txt-delete-title').value=title;
	document.querySelector('#txt-delete-body').value=body;
}

	document.querySelector("#form-delete-post")addEventListener('submit', (e)=>{
	e.preventDefault();

	for (let i = 0; i < posts.length; i++) {
   		 posts[i].remove();
   		}
   		
			

			showPosts(posts);
			alert('Successfully deleted.');

});